package productfx

import (
	product_http "golang-project/service/product/router"

	"go.uber.org/fx"
)

var Module = fx.Provide(provideProductRouter)

func provideProductRouter(
// client *elastic.Client
) product_http.ProductRouter {
	return product_http.NewProductRouter(
	// client,
	)
}
