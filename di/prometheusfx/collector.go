package prometheusfx

import "github.com/prometheus/client_golang/prometheus"

type Collector interface {
	Collect(collectors ...prometheus.Collector)
	UnCollect(collector prometheus.Collector)
}

type registryCollector struct {
	registerer prometheus.Registerer
}

func (r *registryCollector) Collect(collectors ...prometheus.Collector) {
	prometheus.MustRegister(collectors...)
}

func (r *registryCollector) UnCollect(collector prometheus.Collector) {
	prometheus.Unregister(collector)
}
