package prometheusfx

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Provide(providePrometheusMiddleware),
	fx.Invoke(initializeMetricsHandler),
)

func initializeMetricsHandler(lifecycle fx.Lifecycle) {
	g := gin.New()
	g.Any("/metrics", gin.WrapH(promhttp.Handler()))
	lifecycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go registerServer(g)
			return nil
		},
	})
}

func providePrometheusMiddleware() Middleware {
	registerer := prometheus.NewRegistry()
	return NewMiddleware(&registryCollector{registerer: registerer})
}

func registerServer(g *gin.Engine) {
	fmt.Println("expose metrics on port: 9092")
	server := http.Server{
		Addr:    ":9092",
		Handler: g,
	}

	if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal("failed to listen and serve from server: %v", err)
	}
}

// var (
// 	httpDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
// 		Name: "myapp_http_duration_seconds",
// 		Help: "Duration of HTTP requests.",
// 	}, []string{"path"})
// )

// // prometheusMiddleware implements mux.MiddlewareFunc.
// func prometheusMiddleware(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		route := mux.CurrentRoute(r)
// 		path, _ := route.GetPathTemplate()
// 		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
// 		next.ServeHTTP(w, r)
// 		timer.ObserveDuration()
// 	})
// }

// func mains() {
// 	r := mux.NewRouter()
// 	r.Use(prometheusMiddleware)
// 	r.Path("/metrics").Handler(promhttp.Handler())
// 	r.Path("/obj/{id}").HandlerFunc(
// 		func(w http.ResponseWriter, r *http.Request) {})

// 	srv := &http.Server{Addr: "localhost:1234", Handler: r}
// 	srv.ListenAndServe()
// }
