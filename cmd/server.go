package cmd

import (
	"context"
	"errors"
	"fmt"
	"golang-project/config"
	"golang-project/di/productfx"
	"golang-project/di/prometheusfx"
	"golang-project/di/tracingfx"
	product_http "golang-project/service/product/router"
	"golang-project/tracing"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/scram"
	"github.com/spf13/cobra"
	"go.uber.org/fx"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		NewServer().Run()
	},
	Version: "1.0.0",
}

type Server struct{}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) Run() {
	app := fx.New(
		fx.Invoke(config.InitConfig),
		// esfx.Module,
		productfx.Module,
		tracingfx.Module,
		prometheusfx.Module,
		fx.Provide(provideGinEngine),
		fx.Invoke(
			registerService,
		),
		fx.Invoke(StartServer),
	)
	app.Run()
}

func provideGinEngine() *gin.Engine {
	return gin.New()
}

func registerService(
	g *gin.Engine,
	ProductRouter product_http.ProductRouter,
	prometheusMdw prometheusfx.Middleware,
	tracer tracing.Tracer,
) {
	api := g.Group("/api/v1")
	api.Use(prometheusMdw.GetMetrics()...)
	api.Use(tracer.TracingHandler)

	ProductRouter.Register(api)
}

func StartServer(lifecycle fx.Lifecycle, g *gin.Engine) {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				go registerServer(g)
				go registerKafka()
				return nil
			},
		},
	)
}

func registerServer(g *gin.Engine) {
	fmt.Println("run on port:8000")
	server := http.Server{
		Addr:    ":8000",
		Handler: g,
	}

	// arr := []int{1, 2, 3}
	// a := make(chan int)
	// b := make(chan int)
	// c := make(chan int)
	// d := make(chan int)

	// go func() {
	// 	for i := 0; i < len(arr); i++ {
	// 		fmt.Println("============arr[i]", arr[i])
	// 		a <- arr[i]
	// 	}
	// }()

	// fmt.Println("============0")
	// go funcA(<-a, b)
	// go funcB(<-a, c)
	// go funcB(<-a, d)
	// squares, cubes := <-b, <-c
	// fmt.Println("============squares", squares)
	// fmt.Println("============cubes", cubes)

	// _, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	// defer cancel() // defer cancel sau khi span.Finish()
	// time.Sleep(10 * time.Second)

	if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal("failed to listen and serve from server: %v", err)
	}
}

func funcA(a int, b chan int) {
	fmt.Println("==========funcA", a)
	b <- a
}

func funcB(a int, c chan int) {
	fmt.Println("==========funcB", a)
	c <- a
}

func registerKafka() {
	ctx := context.Background()
	// mechanism := plain.Mechanism{
	// 	Username: "NBD7IEDOHEXPYAGM",
	// 	Password: "j47Qm5MIhjFIIk6d5G8hTBWi9uhBdd4VI6Ty0Gln3w0U+9W3al4R7i6Z09WJ2XfY",
	// }

	// dialer := &kafka.Dialer{
	// 	Timeout:       45000,
	// 	DualStack:     true,
	// 	SASLMechanism: mechanism,
	// }

	// conn, err := dialer.Dial("gcp", "pkc-ldvr1.asia-southeast1.gcp.confluent.cloud:9092")
	// fmt.Println("===========conn", conn)
	// fmt.Println("===========err", err)

	mechanism, err := scram.Mechanism(scram.SHA512, "m59rg760", "m_GjZ-EHeFzSHrdPDeMTukm-ZZOLtlc2")
	if err != nil {
		panic(err)
	}

	dialer := &kafka.Dialer{
		Timeout:       10 * time.Second,
		DualStack:     true,
		SASLMechanism: mechanism,
	}

	conn, err := dialer.DialContext(ctx, "tcp", "moped.srvs.cloudkafka.com")
	fmt.Println("===========conn", conn)
	fmt.Println("===========err", err)
	// r := kafka.NewReader(kafka.ReaderConfig{
	// 	Brokers: []string{"pkc-ldvr1.asia-southeast1.gcp.confluent.cloud:9092"},
	// 	Topic:   "Topic1",
	// 	Dialer:  dialer,
	// })
	// r := kafka.NewReader(kafka.ReaderConfig{
	// 	Brokers: []string{"localhost:9092"},
	// 	GroupID: "consumer-group-id",
	// 	Topic:   "Topic2",
	// 	// Partition: 1,
	// 	MinBytes: 10e3, // 10KB
	// 	MaxBytes: 10e6, // 10MB
	// })

	// for {
	// 	m, err := r.FetchMessage(ctx) // FetchMessage does not commit offsets automatically when using consumer groups. Use CommitMessages to commit the offset.
	// 	// m, err := r.ReadMessage(context.Background()) // FetchMessage does not commit offsets automatically when using consumer groups. Use CommitMessages to commit the offset.
	// 	if err != nil {
	// 		fmt.Println("err: ", err)
	// 		break
	// 	}
	// 	fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
	// }

	// if err := r.Close(); err != nil {
	// 	log.Fatal("failed to close reader:", err)
	// }
}
