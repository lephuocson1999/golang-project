# # syntax=docker/dockerfile:1

# FROM golang:1.16-alpine AS build

# WORKDIR /app

# COPY go.mod ./
# COPY go.sum ./
# RUN go mod download

# # COPY *.go ./
# COPY . .

# # RUN go mod init gitlab.com/lephuocson1999/golang-project 
# RUN GOPRIVATE=gitlab.com/lephuocson1999 go mod vendor

# # RUN go env -w GO111MODULE=off

# RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /golang-project 

# EXPOSE 8080

# CMD [ "/golang-project" ]


# FROM golang:1.16-alpine AS build
# WORKDIR /src
# COPY . .
# RUN go get github.com/prometheus/client_golang/prometheus@v1.12.2
# RUN go build -o /golang-project .

# FROM scratch AS bin
# COPY --from=build /golang-project /

# EXPOSE 8081

# CMD [ "/golang-project" ]


FROM golang:1.16-alpine AS builder
WORKDIR /go/src/golang-project/
COPY . .
RUN go get github.com/prometheus/client_golang/prometheus@v1.12.2
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o golang-project .
FROM alpine:latest
WORKDIR /root/
COPY --from=builder /go/src/golang-project .
EXPOSE 8081
CMD ["./golang-project"]