package router

import (
	"encoding/csv"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

type ProductRouter interface {
	Register(routerGroup gin.IRouter)
}

type productRouterIml struct {
	// client *elastic.Client
}

func NewProductRouter(
// client *elastic.Client
) ProductRouter {
	return &productRouterIml{
		// client: client,
	}
}
func (p *productRouterIml) Register(r gin.IRouter) {
	product := r.Group("/products")
	{
		product.GET("/produce", p.ProduceListProduct)
		product.GET("", p.ListProduct)
	}
}

func (p *productRouterIml) ProduceListProduct(c *gin.Context) {
	// make a writer that produces to topic-A, using the least-bytes distribution
	// w := &kafka.Writer{
	// 	Addr:         kafka.TCP("pkc-ldvr1.asia-southeast1.gcp.confluent.cloud:9092"),
	// 	Topic:        "Topic2",
	// 	RequiredAcks: -1,
	// 	Balancer:     &kafka.Hash{},
	// }

	// products := []model.Product{
	// 	{
	// 		ID:   "1",
	// 		Name: "Product_6_1",
	// 	},
	// 	{
	// 		ID:   "2",
	// 		Name: "Product_6_2",
	// 	},
	// 	{
	// 		ID:   "3",
	// 		Name: "Product_6_3",
	// 	},
	// }

	// messages := []kafka.Message{}
	// for _, v := range products {
	// 	data, err := json.Marshal(v)
	// 	if err != nil {
	// 		continue
	// 	}

	// 	messages = append(messages, kafka.Message{
	// 		Key:   []byte("Product6"),
	// 		Value: data,
	// 	})
	// }

	// err := w.WriteMessages(context.Background(), messages...)
	// if err != nil {
	// 	log.Fatal("failed to write messages:", err)
	// }

	// if err := w.Close(); err != nil {
	// 	log.Fatal("failed to close writer:", err)
	// }
	keywords := make(map[string]interface{})
	records := readCsvFile("./Search_keyword.csv")
	for _, v := range records {
		for _, c := range v {
			keywords[c] = true
		}
	}
	// time.Sleep(100 * time.Second)
	// fmt.Println(keywords)

	c.JSON(200, gin.H{
		"message": "done",
		"data":    keywords,
	})
}

func readCsvFile(filePath string) [][]string {
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for "+filePath, err)
	}

	return records
}

func (p *productRouterIml) ListProduct(c *gin.Context) {
	// Use the IndexExists service to check if the index "twitter" exists.

}
